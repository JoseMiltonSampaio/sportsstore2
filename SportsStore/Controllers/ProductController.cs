﻿using Microsoft.AspNetCore.Mvc;
using SportsStore.Models;
using System.Linq;
using SportsStore.Models.ViewModels;

namespace SportsStore.Controllers
{
    public class ProductController : Controller
    {
        private IProductRepository repository;
        public int PageSize = 4;

        public ProductController(IProductRepository repository)
        {
            this.repository = repository;
        }

        public ViewResult List(string category, int productPage = 1)
        {
            var products = repository.Products;

            if (category != null)
            {
                products = products.Where(p => p.Category == null || p.Category == category);
            }

            products = products.OrderBy(p => p.ProductId).Skip((productPage - 1) * PageSize).Take(PageSize);

            return View(new ProductsListViewModel
            {
                Products = products,
                PagingInfo = new PagingInfo { CurrentPage = productPage, ItemsPerPage = PageSize, TotalItems = category == null ? repository.Products.Count() : repository.Products.Where(e => e.Category == category).Count() },
                CurrentCategory = category
            });
        }

          
        //public ViewResult List() => View(repository.Products);
    }
}
